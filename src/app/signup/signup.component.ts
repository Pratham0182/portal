import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http'

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  constructor(private http:HttpClient) { }

  public form ={

  	email:null,
  	password:null,
  	name:null,
  	confirm_password:null,
  };

  onSubmit(){
  
    return this.http.post('http://localhost:8000/api/user_reg',this.form).subscribe(
        data=>console.log(data),
        error=>this.handleError(error),
    );

  }

  handleError(error){
    this.error =error.error.error;
  }

  ngOnInit() {
  }

}
